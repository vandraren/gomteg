# GoMTeG: Google Mail to Telegram Group

## ¿Qué es GoMTeG?:
[@gomteg_bot](https://t.me/gomteg_bot) es un bot de Telegram que tiene como principal objetivo la entrega en una conversación de Telegram de mensajes no leídos de una cuenta de GMail.
El bot accede a la API de Gmail mediante OAuth2, un protocolo de autorización a terceros, para obtener estos correos.

El proyecto comenzó el 15 de Noviembre de 2019, donde se empezó a desarrollar esta idea inicial, que acabó derivando en subsecuentes funcionalidades que la enriquecen. 

## Resumen de versiones
### v1.7 (24/10/2020)
En esta versión se modifica el Procfile de Heroku para ejecutar un dyno web y así evitar uso de recursos cuando no hay peticiones.
En las siguientes subversiones, se introduce un script para evitar el bloqueo de suscripciones desde Heroku, las suscripciones diarias y la personalización de lengua para un chat concreto.

### v1.6 (18/09/2020)
Esta versión introduce la encriptación de los tokens de autorización de la base de datos para dificultar accesos no autorizados.

### v1.5 (14/06/2020)
Esta versión nace con el propósito de que se garantice el cumplimiento de los límites de tráfico establecidos por Telegram.
Para ello, se dedica una nueva clase a la gestión de las entregas de mensajes, que recibe las instrucciones de qué enviar desde los métodos de gestión de actualizaciones y planificación de notificaciones.

### v1.4 (08/04/2020)
Con esta versión, GoMTeG comienza a almacenar las ids de todos los correos que entrega, de forma que no se entreguen repeticiones de correos no leídos que se encuentran todavía en la bandeja de entrada.

Posteriormente, se introduce el concepto de suscripción de notificaciones, que permite que el bot realice de forma automática entregas de mensajes cada cierto tiempo.
Además, el usuario ahora también puede eliminar la autorización de acceso a su cuenta, algo que solo se podía hacer de mano del administrador.

### v1.3 (31/03/2020)
Esta versión introduce la posibilidad de que los usuarios manden mensajes al administrador.

Además, se realiza una gran reestructuración del código correspondiente a los comandos y se incluyen métodos de monitorización y diagnóstico de la ejecución.

### v1.2 (13/03/2020)
Esta versión tuvo como objetivo la integración de GoMTeG en Heroku, para poder funcionar en todo momento.

En posteriores subversiones, se introdujo la posibilidad de utilizar el leonés, además de herramientas para generar documentación para nuevos idiomas.

### v1.1 (08/03/2020) 
Esta versión introduce mejoras en la ayuda al usuario y personalización de lengua, ya sea castellano o inglés, además de empezar a utilizar un entorno virtual y un archivo de dependencias.

### v1.0 (04/03/2020)
En la `v1.0`, se introduce la concesión de autorizaciones mediante intercambio de mensajes, requisito mínimo para la usabilidad de la aplicación para personas fuera del equipo de desarrollo.
