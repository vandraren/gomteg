from os import environ as env
from os import path
from os.path import dirname

ROOT_PATH = dirname(dirname(path.abspath(__file__)))
DATA_PATH = path.join(ROOT_PATH, "data")

DEBUG = env.get('DEBUG', False)
ON_HEROKU = env.get('ON_HEROKU', False)

if ON_HEROKU:
    HEROKU_DOMAIN = 'https://' + env['APP_NAME'] + '.herokuapp.com'
    WEB_DOMAIN = HEROKU_DOMAIN
else:
    WEB_DOMAIN = 'localhost'
