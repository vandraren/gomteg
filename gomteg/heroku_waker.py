from datetime import datetime, timedelta
import requests

from gomteg.db.database import Database
from gomteg.defs import HEROKU_DOMAIN

with Database() as db:
    suscripcion = db.get_siguiente_suscripcion()

if suscripcion and suscripcion["time"] <= datetime.now() + timedelta(minutes=10):
    requests.get(HEROKU_DOMAIN)
