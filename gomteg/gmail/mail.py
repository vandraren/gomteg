import base64
import datetime
import email
from email import header, utils

from gomteg.replies import bot_replies


class MailString(str):
    GMAIL_DOMAIN = '@gmail.com'

    def get_acc(self):
        return self.replace(self.GMAIL_DOMAIN, '')

    def is_gmail_string(self):
        return self.endswith(self.GMAIL_DOMAIN)


class Correo:
    def __init__(self, *, id: str, mensaje):
        self.id = id

        def obtener_mime_msg(message):
            msg_str = base64.urlsafe_b64decode(message['raw'])
            try:
                msg_str = msg_str.decode('UTF-8')
            except UnicodeDecodeError:
                msg_str = msg_str.decode('unicode_escape')

            return email.message_from_string(str(msg_str))

        mime_msg = obtener_mime_msg(mensaje)

        def decodificar_cabecera(cabecera):
            def procesar_cabecera(cabecera):
                def partes_cabecera(cabecera):
                    for elem, encoding in cabecera:
                        if encoding:
                            elem = elem.decode(encoding)
                        yield elem

                cadena = ''
                for elemento in partes_cabecera(cabecera):
                    try:
                        cadena += elemento
                    except TypeError:
                        cadena += elemento.decode()
                return cadena

            cabecera = email.header.decode_header(cabecera or '')
            return procesar_cabecera(cabecera)

        self.remitente = decodificar_cabecera(mime_msg['from'])
        self.fecha = datetime.datetime.fromtimestamp(
            email.utils.mktime_tz(email.utils.parsedate_tz(mime_msg['date']))
        ).strftime("%d-%m-%Y %H:%M:%S")
        self.destinatario = decodificar_cabecera(mime_msg['to'])
        self.asunto = decodificar_cabecera(mime_msg['subject'])

        def procesar_contenido(message):
            def procesar_parte(parte):
                ctype = parte.get_content_type()
                cdispo = str(parte.get('Content-Disposition'))

                # skip any text/plain (txt) attachments
                if ctype == 'text/plain' and 'attachment' not in cdispo:
                    return parte.get_payload(decode=True)  # decode
                else:
                    return ''

            contenido = ""
            if message.is_multipart():
                for part in message.walk():
                    contenido = procesar_parte(part)
                    if contenido:
                        break
            # not multipart - i.e. plain text, no attachments, keeping fingers crossed
            else:
                contenido = procesar_parte(message)

            try:
                contenido = contenido.decode('UTF-8')
            except UnicodeDecodeError:
                contenido = contenido.decode('unicode_escape')
            except AttributeError:
                pass

            contenido = contenido.strip()

            return contenido

        self.contenido = procesar_contenido(mime_msg)

    def __str__(self, lengua: str):
        string = "{}: {}\n" \
                 "{}: {}\n" \
                 "{}: {}\n" \
                 "{}: {}\n". \
            format(
                bot_replies.SENDER[lengua], self.remitente,
                bot_replies.DATE[lengua], self.fecha,
                bot_replies.RECEIVER[lengua], self.destinatario,
                bot_replies.SUBJECT[lengua], self.asunto
            )

        if self.contenido:
            string += "\n"
            if len(string) + len(self.contenido) <= 4096:
                string += str(self.contenido)
            else:
                max_length = 4096 - len(string) - 3
                string += self.contenido[:max_length]
                string += "..."
            string += "\n"

        return string
