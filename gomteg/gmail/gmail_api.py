import oauthlib
from oauthlib import oauth2
import google_auth_oauthlib.flow
from googleapiclient.discovery import build
from google.auth.transport.requests import Request

import pickle
import simplejson as json
import os.path
from enum import Enum


from gomteg.gmail.mail import MailString, Correo

from gomteg.defs import DATA_PATH
from gomteg.db.database import Database


# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/gmail.readonly']

CREDS_PATH = os.path.join(DATA_PATH, "credentials.json")

AUTH_TIMEOUT = 120
MAX_CORREOS = 10


class AuthorizationError(Enum):
    ACCOUNT_WITH_NO_CREDS = 0
    INVALID_AUTH_CODE = 1


class GmailApi:
    def __init__(self, usuario: int = None, cuenta: MailString = None):
        self.autenticado = False

        if usuario and cuenta:
            try:
                self.creds = self.__leer_token(usuario, cuenta)
                self.service = build('gmail', 'v1', credentials=self.creds, cache_discovery=False)
                self.autenticado = True
            except ValueError:
                self.error_code = AuthorizationError.ACCOUNT_WITH_NO_CREDS
        else:
            if not os.path.exists(CREDS_PATH):
                if not os.path.exists(DATA_PATH):
                    os.mkdir(DATA_PATH)
                with Database() as db:
                    credentials = db.get_credentials()
                with open(CREDS_PATH, 'w') as creds_file:
                    json.dump(credentials, creds_file)

            self.flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
                CREDS_PATH, scopes=SCOPES, redirect_uri="urn:ietf:wg:oauth:2.0:oob")
            self.authorization_url, _ = self.flow.authorization_url(access_type='offline')

    '''
    MÉTODOS DE AUTENTICACIÓN
    '''

    @staticmethod
    def __leer_token(usuario, cuenta):
        creds = None

        with Database() as db:
            if db.existe_registro(usuario, cuenta):
                creds = pickle.loads(db.obtener_pickle(usuario, cuenta))

        if creds:
            if not creds.valid and creds.expired and creds.refresh_token:
                creds.refresh(Request())
                with Database() as db:
                    db.actualizar_pickle(usuario, cuenta, pickle.dumps(creds))
        else:
            raise ValueError

        return creds

    def autorizar(self, code):
        try:
            self.flow.fetch_token(code=code)
            if self.flow.credentials:
                creds = self.flow.credentials

                self.service = build('gmail', 'v1', credentials=creds)
                cuenta = self.__obtener_cuenta()
                return cuenta, pickle.dumps(creds)
            else:
                return None, None
        except oauthlib.oauth2.rfc6749.errors.InvalidGrantError:
            self.error_code = AuthorizationError.INVALID_AUTH_CODE
            return None, None

    '''
    MÉTODOS DE LA API
    '''

    def procesar_correos(self, *, chat_id: int, num_correos: int = None, disable_max: bool = False):
        email = MailString(self.__obtener_cuenta())

        # Obtención de leídos
        leidos = self.__obtener_leidos(chat_id, email)

        # Obtención de correos no leídos, filtrando los ya leídos por el bot
        no_leidos = [correo['id'] for correo in self.__obtener_correos_no_leidos() or []]
        ids = [id for id in no_leidos if id not in leidos]

        # Si hay leídos que no se reciben en la lectura inicial, se eliminan de la BD
        obsoletos = [id for id in leidos if id not in no_leidos]
        if obsoletos:
            self.__suprimir_obsoletos(obsoletos, chat_id, email)

        # Gestión del número de correos a mostrar
        if num_correos is not None:
            ids = ids[:min(num_correos, len(ids))]
        elif len(ids) > MAX_CORREOS and not disable_max:
            yield None
            return

        # Obtención de correos a partir de ids
        for id in ids or []:
            yield Correo(id=id, mensaje=self.__obtener_correo(id))

    def __obtener_cuenta(self):
        peticion_usuario = self.service.users().getProfile(userId='me')
        resultados = peticion_usuario.execute()
        return resultados["emailAddress"]

    @staticmethod
    def __obtener_leidos(chat_id, email):
        with Database() as db:
            if db.existe_chat(chat_id, email):
                leidos = db.get_leidos(chat_id, email)
            else:
                db.insertar_chat(chat_id, email)
                leidos = []
        return leidos

    def __obtener_correos_no_leidos(self):
        peticion_mensajes = self.service.users().messages().list(userId='me', labelIds=['UNREAD'])
        resultados = peticion_mensajes.execute()
        correos = resultados.get("messages")
        return correos

    @staticmethod
    def __suprimir_obsoletos(leidos, chat_id, email):
        with Database() as db:
            for leido in leidos:
                db.suprimir_leido(chat_id, email, leido)

    def __obtener_correo(self, message_id):
        peticion_mensaje = self.service.users().messages().get(userId='me', id=message_id, format='raw')
        correo = peticion_mensaje.execute()
        return correo


if __name__ == "__main__":
    pass
