from telegram.ext import CommandHandler, Updater, MessageHandler, Filters

from os import environ as env
import logging
from threading import Thread

from gomteg.defs import ON_HEROKU, WEB_DOMAIN
from gomteg.commands.callbacks import Start, Help, SetLanguage, Subscribe, Unsubscribe, SignUp, \
    Check, Echo, Unknown, Contact, Delete, SubscriptionsThread
from gomteg.commands.help import Commands
from gomteg.messages import MessagesThread


class TelegramBot:
    def __init__(self):
        bot_token = env["GOMTEG_API_TOKEN"]
        self.updater = Updater(token=bot_token, use_context=True)

        if ON_HEROKU:
            self.updater.start_webhook(listen="0.0.0.0", port=int(env.get('PORT', '8443')), url_path=bot_token)
            self.updater.bot.set_webhook(WEB_DOMAIN + "/" + bot_token)

        self.messages_thread = MessagesThread(bot=self.updater.bot)
        self.messenger = self.messages_thread.messenger
        self.polling_thread = Thread(target=self.updater.start_polling)
        self.subscriptions_thread = SubscriptionsThread(self.messenger)

        start = Start(self.messenger)
        help_cmd = Help(self.messenger)
        contact = Contact(self.messenger)
        signup = SignUp(self.messenger)
        delete = Delete(self.messenger)
        setlanguage = SetLanguage(self.messenger)
        subscribe = Subscribe(self.messenger, self.subscriptions_thread)
        unsubscribe = Unsubscribe(self.messenger)
        check = Check(self.messenger)
        echo = Echo(self.messenger)
        unknown = Unknown(self.messenger)

        self.updater.dispatcher.add_handler(
            CommandHandler(Commands.START.value, start.run)
        )
        self.updater.dispatcher.add_handler(
            CommandHandler(Commands.HELP.value, help_cmd.run)
        )
        self.updater.dispatcher.add_handler(
            CommandHandler(Commands.CONTACT.value, contact.run)
        )
        self.updater.dispatcher.add_handler(
            CommandHandler(Commands.SIGN_UP.value, signup.run)
        )
        self.updater.dispatcher.add_handler(
            CommandHandler(Commands.DELETE.value, delete.run)
        )
        self.updater.dispatcher.add_handler(
            CommandHandler(Commands.SET_LANGUAGE.value, setlanguage.run)
        )
        self.updater.dispatcher.add_handler(
            CommandHandler(Commands.SUBSCRIBE.value, subscribe.run)
        )
        self.updater.dispatcher.add_handler(
            CommandHandler(Commands.UNSUBSCRIBE.value, unsubscribe.run)
        )
        self.updater.dispatcher.add_handler(
            CommandHandler(Commands.CHECK.value, check.run)
        )
        self.updater.dispatcher.add_handler(
            MessageHandler(Filters.command, unknown.run)
        )
        self.updater.dispatcher.add_handler(
            MessageHandler(Filters.text, echo.run)
        )

    def iniciar(self):
        self.messages_thread.start()
        self.polling_thread.start()
        self.subscriptions_thread.start()


if __name__ == "__main__":
    logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=logging.ERROR)

    gomteg_bot = TelegramBot()
    gomteg_bot.iniciar()
