import telegram
import telegram.ext

import dns
import pymongo
from pymongo.errors import ConnectionFailure, InvalidURI, ConfigurationError
from os import environ as env
from bson.objectid import ObjectId
from datetime import datetime, timedelta
import pickle
from cryptography.fernet import Fernet

from gomteg.db.fake_context import FakeCallbackContext

from gomteg.replies.languages import Languages
from gomteg.gmail.mail import MailString


class Database:
    MONGODB_URI = env["MONGODB_URI"]
    cipher = Fernet(env["PRIVATE_KEY"].encode())

    def __init__(self):
        try:
            self.client = pymongo.MongoClient(self.MONGODB_URI)
        except ConfigurationError as e:
            if e.__context__.__class__ == dns.exception.Timeout:
                print("Tiempo de espera superado para la conexión.")
                raise e.__context__
            else:
                print("Error de configuración. ", e)
                raise e

        try:
            pymongo.uri_parser.parse_uri(self.MONGODB_URI, warn=True)
            self.client.admin.command("ismaster")
        except InvalidURI as e:
            print("URI inválido:", self.MONGODB_URI)
            raise e
        except ConnectionFailure as e:
            print("Error de conexión.")
            raise e

        self.db = self.client.gomteg_db
        self.users = self.db.users
        self.chats = self.db.chats
        self.contact = self.db.contact_messages

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.client.close()

    '''
    CHATS
    '''

    def existe_chat(self, chat_id: int, mail: MailString):
        assert isinstance(chat_id, int)
        assert isinstance(mail, MailString)

        return self.chats.count_documents({"chat_id": chat_id, "email": mail.get_acc()}) >= 1

    def insertar_chat(self, chat_id: int, mail: MailString):
        assert isinstance(chat_id, int)
        assert isinstance(mail, MailString)

        if not self.existe_chat(chat_id, mail):
            self.chats.insert_one(
                {"chat_id": chat_id, "email": mail.get_acc(), "messages_read": []}
            )
            return True
        else:
            return False

    def borrar_chat(self, chat_id: int, mail: MailString):
        assert isinstance(chat_id, int)
        assert isinstance(mail, MailString)

        self.chats.delete_one({"chat_id": chat_id, "email": mail.get_acc()})

    '''
    CHATS:LEÍDOS
    '''

    def annadir_leido(self, chat_id: int, mail: MailString, correo_leido: str):
        assert isinstance(chat_id, int)
        assert isinstance(mail, MailString)
        assert isinstance(correo_leido, str)

        self.chats.update_one(
            {"chat_id": chat_id, "email": mail.get_acc()},
            {"$push": {"messages_read": correo_leido}}
        )

    def suprimir_leido(self, chat_id: int, mail: MailString, correo_leido: str):
        assert isinstance(chat_id, int)
        assert isinstance(mail, MailString)
        assert isinstance(correo_leido, str)

        self.chats.update_one(
            {"chat_id": chat_id, "email": mail.get_acc()},
            {"$pull": {"messages_read": correo_leido}}
        )

    def get_leidos(self, chat_id: int, mail: MailString):
        assert isinstance(chat_id, int)
        assert isinstance(mail, MailString)

        suscripcion = self.chats.find_one({"chat_id": chat_id, "email": mail.get_acc()})
        return suscripcion["messages_read"]

    '''
    CHATS:SUBSCRIPTIONS
    '''

    def existe_suscripcion(self, chat_id: int, mail: MailString):
        assert isinstance(chat_id, int)
        assert isinstance(mail, MailString)

        chat_con_suscripcion = {
            "chat_id": chat_id,
            "email": mail.get_acc(),
            "suscripción": {"$exists": True}
        }
        return self.chats.count_documents(chat_con_suscripcion) >= 1

    def insertar_suscripcion(self, chat_id: int, mail: MailString, frequency: dict,
                             update: telegram.Update, context: telegram.ext.CallbackContext):
        def exportar_time_period(time_period):
            return {
                "days": time_period.days,
                "seconds": time_period.seconds,
                "microseconds": time_period.microseconds
            }

        assert isinstance(chat_id, int)
        assert isinstance(mail, MailString)
        assert isinstance(frequency, dict)
        assert isinstance(update, telegram.Update)
        assert isinstance(context, telegram.ext.CallbackContext)

        if not self.existe_chat(chat_id, mail):
            self.insertar_chat(chat_id, mail)

        fake_context = FakeCallbackContext(context, args=[context.args[0]])

        if frequency.pop('tipo') == 't':
            frequency = timedelta(**frequency)
            next_notification = datetime.now() + frequency
        else:
            now = datetime.now()
            next_notification = datetime(
                **{
                    'year': now.year,
                    'month': now.month,
                    'day': now.day,
                    'hour': frequency['hour'],
                    'minute': frequency['minute']
                }
            )
            frequency = timedelta(days=1)

            if next_notification < now:
                next_notification = next_notification + frequency

        suscripcion = {
            "update_info": pickle.dumps((update, fake_context)),
            "frequency": exportar_time_period(frequency),
            "next_notification": next_notification
        }

        if not self.existe_suscripcion(chat_id, mail):
            self.chats.update_one(
                {"chat_id": chat_id, "email": mail.get_acc()},
                {"$set": {"suscripción": suscripcion}}
            )
            return True
        else:
            return False

    def get_siguiente_suscripcion(self):
        try:
            chat = self.chats.find(
                {"suscripción": {"$exists": True}}).sort("suscripción.next_notification", pymongo.ASCENDING)[0]
        except IndexError:
            return None

        chat_id = chat["chat_id"]
        email = chat["email"]
        suscripcion = chat["suscripción"]

        notification_time = suscripcion["next_notification"]
        update, context = pickle.loads(suscripcion["update_info"])

        return {
            "chat_id": chat_id,
            "email": email,
            "time": notification_time,
            "update": update,
            "context": context
        }

    def actualizar_suscripcion(self, chat_id: int, mail: MailString):
        assert isinstance(chat_id, int)
        assert isinstance(mail, MailString)

        chat = self.chats.find_one({"chat_id": chat_id, "email": mail.get_acc()})

        try:
            suscripcion = chat["suscripción"]
            frequency = timedelta(**suscripcion["frequency"])
            next_notification = suscripcion["next_notification"]
        except KeyError or TypeError:
            return

        while next_notification < datetime.now():
            next_notification += frequency

        self.chats.update_one(
            {"chat_id": chat_id, "email": mail.get_acc()},
            {"$set": {"suscripción.next_notification": next_notification}}
        )

    def borrar_suscripcion(self, chat_id: int, mail: MailString):
        assert isinstance(chat_id, int)
        assert isinstance(mail, MailString)

        self.chats.update_one(
            {"chat_id": chat_id, "email": mail.get_acc()},
            {"$unset": {"suscripción": ""}}
        )

    '''
    USERS:REGISTROS
    '''

    def existe_registro(self, usuario: int, mail: MailString):
        assert isinstance(usuario, int)
        assert isinstance(mail, MailString)

        registro = {"usuario": usuario, "mails.{}".format(mail.get_acc()): {"$exists": True}}
        return self.users.count_documents(registro) > 0

    def existen_registros(self, usuario: int):
        assert isinstance(usuario, int)

        registros = {"usuario": usuario, "mails": {"$exists": True}}
        return self.users.count_documents(registros) > 0

    def insertar_registro(self, usuario: int, mail: MailString, pickle):
        assert isinstance(usuario, int)
        assert isinstance(mail, MailString)

        entrada_usuario = {"usuario": usuario}
        if self.users.count_documents(entrada_usuario) > 0:
            if self.existe_registro(usuario, mail):
                return False
            else:
                self.users.update_one(
                    {"usuario": usuario}, {"$set": {"mails.{}".format(mail.get_acc()): self.cipher.encrypt(pickle)}}
                )
        else:
            self.users.insert_one(
                {"usuario": usuario, "mails": {mail.get_acc(): self.cipher.encrypt(pickle)}}
            )
        return True

    def actualizar_pickle(self, usuario: int, mail: MailString, pickle):
        assert isinstance(usuario, int)
        assert isinstance(mail, MailString)

        if self.existe_registro(usuario, mail):
            self.users.update_one(
                {"usuario": usuario}, {"$set": {"mails.{}".format(mail.get_acc()): self.cipher.encrypt(pickle)}}
            )
            return True
        return False

    def obtener_pickle(self, usuario: int, mail: MailString):
        assert isinstance(usuario, int)
        assert isinstance(mail, MailString)

        if self.existe_registro(usuario, mail):
            usuario = self.users.find_one({"usuario": usuario})
            pickle = usuario["mails"][mail.get_acc()]
            return self.cipher.decrypt(pickle)
        else:
            return None

    def borrar_registro(self, usuario: int, mail: MailString):
        assert isinstance(usuario, int)
        assert isinstance(mail, MailString)

        self.users.update_one(
            {"usuario": usuario}, {"$unset": {"mails": mail.get_acc()}}
        )

    def borrar_registros(self, usuario: int):
        assert isinstance(usuario, int)

        self.users.update_one(
            {"usuario": usuario}, {"$unset": {"mails": 1}}
        )

    def obtener_registros(self, usuario: int):
        usuario = self.users.find_one({
            'usuario': usuario,
            'mails': {"$exists": True}
        })
        return None if not usuario else usuario['mails']

    '''
    USERS:OTROS
    '''

    def set_lengua(self, usuario: int, lengua: Languages, chat_id: int = None):
        assert isinstance(usuario, int)
        assert isinstance(lengua, Languages)

        entrada_usuario = {'usuario': usuario}
        if chat_id:
            assert isinstance(chat_id, int)

            if self.__get_lengua(usuario):
                config_chat = {'chat_id': chat_id, 'usuario': usuario}
                if self.chats.count_documents(config_chat) > 0:
                    self.chats.update_one(
                        config_chat,
                        {'$set': {'lengua': lengua.value}}
                    )
                else:
                    self.chats.insert_one({
                        'chat_id': chat_id,
                        'usuario': usuario,
                        'lengua': lengua.value
                    })
            else:
                return False
        elif self.users.count_documents(entrada_usuario) > 0:
            self.users.update_one(
                entrada_usuario, {"$set": {"lengua": lengua.value}}
            )
        else:
            self.users.insert_one(
                {"usuario": usuario, "lengua": lengua.value}
            )
        return True

    def get_lengua(self, usuario: int, chat_id: int):
        assert isinstance(usuario, int)
        assert isinstance(chat_id, int)

        chat = self.chats.find_one({'chat_id': chat_id, 'usuario': usuario})
        return Languages(chat['lengua']) if chat and 'lengua' in chat else self.__get_lengua(usuario)

    def __get_lengua(self, usuario: int):
        assert isinstance(usuario, int)

        usuario = self.users.find_one({'usuario': usuario, 'lengua': {'$exists': True}})
        if usuario:
            return Languages(usuario['lengua'])
        else:
            return None

    def marcar_como_leidos(self, usuario: int, marcar_leidos: bool):
        assert isinstance(usuario, int)
        assert isinstance(marcar_leidos, bool)

        entrada_usuario = {"usuario": usuario}
        if self.users.count_documents(entrada_usuario) > 0:
            self.users.update_one(
                {"usuario": usuario}, {"$set": {"marcar_leidos": marcar_leidos}}
            )
        else:
            self.users.insert_one(
                {"usuario": usuario, "marcar_leidos": marcar_leidos}
            )

    '''
    CONTACT MESSAGES
    '''

    def insertar_contacto(self, usuario: int, id_mensaje: int, chat_id: int, mensaje: str):
        assert isinstance(usuario, int)
        assert isinstance(id_mensaje, int)
        assert isinstance(chat_id, int)
        assert isinstance(mensaje, str)

        response = self.contact.insert_one(
            {"usuario": usuario,
             "chat_id": chat_id,
             "replied": False,
             "reply_id": id_mensaje,
             "msg": mensaje}
        )

        return response.inserted_id

    def obtener_contacto(self, contact_id: str):
        assert isinstance(contact_id, str)

        return self.contact.find_one({"_id": ObjectId(contact_id)})

    def resolver_contacto(self, contact_id: str):
        assert isinstance(contact_id, str)

        self.contact.update_one(
            {"_id": ObjectId(contact_id)}, {"$set": {"replied": True}}
        )

    '''
    APP CREDENTIALS
    '''

    def get_credentials(self):
        gmail_creds = self.users.find_one({"_id": ObjectId(env["GMAIL_CREDENTIAL_ID"])})
        return gmail_creds["content"]


if __name__ == "__main__":
    pass
