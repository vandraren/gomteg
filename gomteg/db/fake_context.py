from telegram.ext import CallbackContext

from typing import List


class FakeCallbackContext:
    """
        Replicación de atributos de la clase telegram.ext.CallbackContext.
        Fuente: https://python-telegram-bot.readthedocs.io/en/stable/telegram.ext.callbackcontext.html
    """

    def __init__(self, context: CallbackContext, *, args: List = None):
        self.bot_data = context.bot_data
        self.chat_data = context.chat_data
        self.user_data = context.user_data

        self.match = context.match
        self.matches = context.matches

        if not args:
            self.args = context.args
        else:
            self.args = args

        self.error = context.error
        self.job = context.job
        self.bot = context.bot
