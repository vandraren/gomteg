from threading import Thread


class RestartableThread(Thread):
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        super().__init__(*args, **kwargs)

    def restart(self):
        new_thread = self.__clone()
        new_thread.start()
        return new_thread

    def __clone(self):
        return type(self)(*self.args, **self.kwargs)
