import telegram

from os import environ as env
from threading import Event
from datetime import datetime, timedelta
import time

from gomteg.defs import DEBUG
from gomteg.db.database import Database
from gomteg.gmail.mail import MailString
from gomteg.threads import RestartableThread


class Mensaje:
    def __init__(self, *args, **kwargs):
        self.kwargs = kwargs
        self.chat_id = kwargs['chat_id']

        if args:
            self.mail, self.id = args

    def info(self):
        return self.chat_id, self.mail, self.id


class Log:
    chat_id = env['ADMIN_CHAT_ID']

    def __init__(self, cadena: str):
        self.cadena = cadena


class Espera:
    def __init__(self, chat_id, tiempo):
        self.chat_id = chat_id
        self.tiempo = tiempo

    def espera(self):
        espera = self.tiempo - datetime.now()
        return max(espera.total_seconds(), 0)


class Cache:
    buffer_size = 30
    limite = timedelta(seconds=1)

    def __init__(self):
        self.mensajes = []

    def annadir_mensaje(self, mensaje):
        self.mensajes.append(mensaje)
        self.__clear()

    def full(self):
        return len(self.mensajes) >= self.buffer_size

    def ventana(self):
        return self.mensajes[-self.buffer_size]

    def busy(self):
        return datetime.now() - self.ventana() < self.limite

    def t_min(self):
        if self.busy():
            return self.ventana() + self.limite
        else:
            return datetime.now()

    def __clear(self):
        self.mensajes = self.mensajes[-self.buffer_size:]


class Chat(Cache):
    def __init__(self, tipo, mensajes=None):
        super().__init__()
        self.mensajes = mensajes if mensajes else []

        self.tipo = tipo

        self.buffer_size = 20 if self.es_grupo() else 1
        if self.es_grupo():
            self.limite = timedelta(minutes=1)

    def es_grupo(self):
        return self.tipo == telegram.Chat.GROUP


class ChatCache:
    def __init__(self):
        self.chats = {}

    def get(self, chat_id):
        try:
            return self.chats[chat_id]
        except KeyError:
            return False

    def set(self, chat_id, chat: Chat):
        self.chats[chat_id] = chat


class Messenger:
    def __init__(self, bot: telegram.Bot, thread):
        self.__bot = bot
        self.__thread = thread

        self.__queue = []
        self.__cache = Cache()
        self.__chat_cache = ChatCache()
        self.__wait_lock = False

    def execute(self):
        while self.__thread.is_alive():
            if self.__queue:
                tarea = self.__sig_tarea()

                if type(tarea) is Mensaje:
                    self.__send_message(**tarea.kwargs)
                    if hasattr(tarea, 'id'):
                        with Database() as db:
                            db.annadir_leido(*tarea.info())
                elif type(tarea) is Log:
                    self.__log(tarea.cadena)
                else:
                    self.__thread.event.clear()
                    self.__thread.event.wait(timeout=tarea.espera())
                    self.__wait_lock = False
            else:
                self.__thread.event.clear()
                self.__thread.event.wait()

    def send_message(self, *, mail: MailString = None, id: str = None, **kwargs):
        args = (mail, id) if mail and id else ()
        self.__annadir_tarea(Mensaje(*args, **kwargs))

    def log(self, cadena: str):
        self.__annadir_tarea(Log(cadena))

    def __annadir_tarea(self, tarea):
        self.__queue.append(tarea)
        if not self.__wait_lock:
            self.__thread.event.set()

    def __sig_tarea(self):
        tarea = None
        for sig in self.__queue:
            if tarea and sig.chat_id == tarea.chat_id:
                break

            t_min, self.__wait_lock = self.__calcular_t_min(sig)

            if t_min <= datetime.now():
                self.__queue.remove(sig)
                return sig
            elif not tarea or tarea and t_min < tarea.tiempo or self.__wait_lock:
                tarea = Espera(sig.chat_id, t_min)
                if self.__wait_lock:
                    break

        return tarea

    def __calcular_t_min(self, sig):
        chat_id = sig.chat_id

        if self.__cache.full() and self.__cache.busy():
            return self.__cache.t_min(), True
        else:
            chat: Chat = self.__chat_cache.get(chat_id)
            t_min = chat.t_min() if chat else datetime.now()
            return t_min, False

    def __send_message(self, **kwargs):
        message = None
        done = False
        while not done:
            try:
                message = self.__bot.send_message(**kwargs)
                done = True
            except telegram.error.TimedOut:
                self.__log("Se acaba de experimentar un timeout al intentar entregar un mensaje.")
            except telegram.error.RetryAfter as e:
                espera = e.retry_after
                self.__log("Se acaba de exceder el límite de desbordamiento. "
                           "Esperando {}s para reintentar...".format(espera))
                time.sleep(espera)

        self.__actualizar_cache(kwargs, message)

    def __actualizar_cache(self, kwargs, message):
        now = datetime.now()
        self.__cache.annadir_mensaje(now)

        chat_id = kwargs['chat_id']
        chat: Chat = self.__chat_cache.get(chat_id)
        if not chat:
            chat = Chat(message.chat.type)
            self.__chat_cache.set(chat_id, chat)
        else:
            chat = self.__chat_cache.get(chat_id)

        chat.annadir_mensaje(now)

    def __log(self, cadena: str):
        if not DEBUG:
            kwargs = {'chat_id': env["ADMIN_CHAT_ID"], 'text': cadena}
            self.__send_message(**kwargs)
        else:
            print(cadena)


class MessagesThread(RestartableThread):
    def __init__(self, *args, **kwargs):
        bot = kwargs['bot']

        self.messenger = Messenger(bot, self)
        self.event = Event()

        super().__init__(target=self.messenger.execute)
        self.args = args
        self.kwargs = kwargs
