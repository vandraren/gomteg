import telegram
import telegram.ext

from os import environ as env
import re
from abc import ABC, abstractmethod
from datetime import datetime
from apscheduler.schedulers.background import BackgroundScheduler

from gomteg.commands.help import Commands, CommandHelper, SYNTAX_ERROR

from gomteg.messages import Messenger
from gomteg.db.database import Database
from gomteg.replies import bot_replies
from gomteg.replies.languages import Languages
from gomteg.gmail.gmail_api import GmailApi, AuthorizationError, MAX_CORREOS
from gomteg.gmail.mail import MailString
from gomteg.threads import RestartableThread


class Command(ABC):
    def __init__(self, messenger: Messenger):
        self.messenger = messenger

    @abstractmethod
    def run(self, update: telegram.Update, context: telegram.ext.CallbackContext):
        pass

    def _log(self, cadena):
        self.messenger.log(cadena)

    def _update_vars(self, update):
        usuario = update.message.from_user
        chat_id = update.effective_chat.id
        lengua = self._obtener_lengua(usuario, chat_id)

        return usuario, chat_id, lengua

    @staticmethod
    def _obtener_lengua(usuario: telegram.User, chat_id: int):
        with Database() as db:
            lengua = db.get_lengua(usuario.id, chat_id)
            if not lengua:
                if usuario.language_code not in [item.value for item in Languages]:
                    lengua = Languages.ENGLISH
                else:
                    lengua = Languages(usuario.language_code)
        return lengua

    @staticmethod
    def _mencion_usuario(usuario: telegram.User):
        if usuario.username:
            mencion = "@{}".format(usuario.username)
        else:
            mencion = usuario.first_name
        return mencion

    def _send_message(self, update: telegram.Update = None, *,
                      msg: str, markdown: bool = False,
                      chat_id: int = None, mail: MailString = None, id: int = None,
                      reply: bool = True, reply_msg_id: int = None):
        kwargs = {"text": msg}

        if mail and id:
            kwargs['mail'] = mail
            kwargs['id'] = id

        if update:
            kwargs["chat_id"] = update.effective_chat.id
            if reply:
                kwargs["reply_to_message_id"] = update.message.message_id
        elif chat_id is not None:
            kwargs["chat_id"] = chat_id
            if reply and reply_msg_id is not None:
                kwargs["reply_to_message_id"] = reply_msg_id
        else:
            raise ValueError("si no hay update, tiene que introducirse un chat_id")

        if markdown:
            kwargs["parse_mode"] = telegram.ParseMode.MARKDOWN

        self.messenger.send_message(**kwargs)

    def _syntax_error(self, update: telegram.Update, lengua: Languages):
        error = SYNTAX_ERROR[lengua.value] + " \n" + \
                CommandHelper.get(Commands(self.__class__.__name__.lower())).help(lengua)
        self._send_message(update, msg=error, markdown=True)

    def _procesar_authorization_error(self, error_code: AuthorizationError, update: telegram.Update,
                                      lengua: str, mail: MailString = None):
        usuario = update.message.from_user.id
        if error_code == AuthorizationError.INVALID_AUTH_CODE:
            error = bot_replies.INVALID_AUTH_CODE[lengua]
            self._log("El usuario {} proporcionó un código de autorización inválido.".format(usuario))
        elif error_code == AuthorizationError.ACCOUNT_WITH_NO_CREDS:
            error = bot_replies.MAIL_WITHOUT_CREDS[lengua].format(mail)
            self._log("El usuario {} no tiene credenciales para el correo {}, "
                      "a pesar de estar registrado.".format(usuario, mail))
        else:
            error = bot_replies.AUTHORIZATION_ERROR[lengua]
            self._log("El usuario {} sufrió un error de autorización desconocido.".format(usuario))
        self._send_message(update, msg=error)


class Start(Command):
    def run(self, update: telegram.Update, context: telegram.ext.CallbackContext):
        usuario, chat_id, lengua = self._update_vars(update)

        self._send_message(update, msg=bot_replies.START_REPLY[lengua.value], markdown=True)
        self._log("El usuario {} inició conversación.\n\n"
                  "{}.".format(self._mencion_usuario(usuario), str(usuario)))


class Help(Command):
    def run(self, update: telegram.Update, context: telegram.ext.CallbackContext):
        usuario, chat_id, lengua = self._update_vars(update)

        if len(context.args) > 0:
            comando = context.args[0]
            if comando in [item.value for item in Commands]:
                cadena_ayuda = CommandHelper.get(Commands(comando)).help(lengua)
            else:
                cadena_ayuda = bot_replies.COMMAND_NOT_AVAILABLE[lengua.value].format(comando)
        else:
            cadena_ayuda = "*" + bot_replies.COMMAND_LIST[lengua.value] + ":*\n"
            for comando in [item.value for item in CommandHelper]:
                cadena_ayuda += "    */{}* - {}\n".format(comando.command.value, comando.descripcion[lengua.value])

            cadena_ayuda += "\n*" + bot_replies.ARGUMENT_SECTION[lengua.value] + ":*\n"
            for argumento in bot_replies.ARGUMENT_TYPES[lengua.value]:
                cadena_ayuda += "    " + argumento + "\n"

        self._send_message(update, msg=cadena_ayuda, markdown=True)


class SetLanguage(Command):
    def run(self, update: telegram.Update, context: telegram.ext.CallbackContext):
        usuario, chat_id, lengua = self._update_vars(update)

        num_args = len(context.args)
        if 1 <= num_args <= 2:
            nueva_lengua = context.args[0]
            if nueva_lengua in [item.value for item in Languages]:
                with Database() as db:
                    if num_args == 1:
                        db.set_lengua(usuario.id, Languages(nueva_lengua))
                        cadena_lengua = bot_replies.NEW_LANGUAGE_SET[nueva_lengua].format(lengua.value, nueva_lengua)
                    elif num_args == 2 and context.args[1] == '-chat':
                        if db.set_lengua(usuario.id, Languages(nueva_lengua), chat_id):
                            cadena_lengua = \
                                bot_replies.CHAT_LANGUAGE_SET[nueva_lengua].format(lengua.value, nueva_lengua)
                        else:
                            cadena_lengua = bot_replies.NO_LANGUAGE_SET[lengua.value]
            else:
                cadena_lengua = bot_replies.LANGUAGE_NOT_IMPLEMENTED[lengua.value].format(nueva_lengua) + "\n\n" + \
                                SYNTAX_ERROR[lengua.value] + " \n" + \
                                CommandHelper.get(Commands.SET_LANGUAGE).help(lengua)
            self._send_message(update, msg=cadena_lengua, markdown=True)
        else:
            self._syntax_error(update, lengua)


class Contact(Command):
    def __init__(self, messenger: Messenger):
        super().__init__(messenger)

        # Patterns allowed:
        #       /contact -id [id] -msg <msg>
        #       /contact <msg>
        self.pattern = re.compile(r"/contact(?:@\w*)?\s(?:-id\s([a-z0-9]*)\s-msg\s)?(.*)")

    def run(self, update: telegram.Update, context: telegram.ext.CallbackContext):
        contact_id, mensaje_recibido = self.__procesar_entrada(str(update.message.text))
        usuario, chat_id, lengua = self._update_vars(update)

        if mensaje_recibido is not None:
            admin_chat_id = int(env["ADMIN_CHAT_ID"])
            if not contact_id or chat_id != admin_chat_id:
                self.__mandar_contacto(update, lengua, chat_id, admin_chat_id, usuario.id, mensaje_recibido)
            else:
                self.__responder_contacto(update, lengua.value, contact_id, mensaje_recibido)
        else:
            self._syntax_error(update, lengua)

    def __procesar_entrada(self, entrada: str):
        match = self.pattern.match(entrada)
        if match:
            if not match.group(1):
                contact_id = None
            else:
                contact_id = match.group(1)
            mensaje_recibido = match.group(2)

            return contact_id, mensaje_recibido
        else:
            return None, None

    def __mandar_contacto(self, update, lengua: Languages, chat_id, admin_chat_id, user_id, contenido):
        usuario = self._mencion_usuario(update.message.from_user)
        msg_id = update.message.message_id

        with Database() as db:
            contact_id = db.insertar_contacto(user_id, msg_id, chat_id, contenido)
            db.set_lengua(user_id, lengua)
        contact_msg = "ID del contacto: {}\n" \
                      "Usuario: {}\n" \
                      "ID: {}\n" \
                      "Mensaje: {}".format(contact_id, usuario, user_id, contenido)

        self._send_message(chat_id=admin_chat_id, msg=contact_msg)
        self._send_message(update, msg=bot_replies.CONTACT_CONFIRMATION[lengua.value])

    def __responder_contacto(self, update, lengua: str, contact_id, contenido):
        with Database() as db:
            contacto = db.obtener_contacto(contact_id)
            db.resolver_contacto(contact_id)
        self._send_message(chat_id=contacto["chat_id"], msg=contenido, reply_msg_id=contacto["reply_id"])
        self._send_message(update, msg=bot_replies.CONTACT_CONFIRMATION[lengua])


class SignUp(Command):
    def __init__(self, messenger):
        super().__init__(messenger)

    def run(self, update: telegram.Update, context: telegram.ext.CallbackContext):
        usuario, chat_id, lengua = self._update_vars(update)

        if len(context.args) == 0:
            self.__crear_peticion(update, context.user_data, lengua.value)
        elif len(context.args) == 1:
            codigo = context.args[0]
            self.__completar_peticion(update, context.user_data, codigo, usuario.id, lengua.value)

    def __crear_peticion(self, update, user_data, lengua: str):
        if 'pending_auth' not in user_data:
            try:
                gm_api = GmailApi()
                user_data['pending_auth'] = gm_api

                self._send_message(update, msg=bot_replies.SIGNING_UP_1[lengua].format(gm_api.authorization_url))
                self._send_message(update, msg="\t"+bot_replies.SIGNING_UP_2[lengua].format(
                    Commands.SIGN_UP.value, CommandHelper.get(Commands.SIGN_UP).args[0][lengua]),
                                   markdown=True)
            except TypeError:
                self._send_message(update, msg=bot_replies.AUTH_CREDS_NOT_FOUND[lengua])
                self._log("No se pudieron encontrar las credenciales de Gmail para una nueva autorización.")
        else:
            self._send_message(update, msg=bot_replies.ALREADY_SIGNING_UP[lengua])

    def __completar_peticion(self, update, user_data, codigo, usuario, lengua: Languages):
        if 'pending_auth' in user_data:
            gm_api = user_data['pending_auth']
            mail_a_registrar, pickle = gm_api.autorizar(codigo)
            mail_a_registrar = MailString(mail_a_registrar)
            if pickle:
                user_data.pop('pending_auth')
                with Database() as db:
                    exito_registro = db.insertar_registro(usuario, mail_a_registrar, pickle)
                    db.set_lengua(usuario, lengua)

                if exito_registro:
                    cadena_registro = bot_replies.SIGNED_UP[lengua.value].format(mail_a_registrar)
                    self._log("El usuario {} realizó un nuevo registro.".format(usuario))
                else:
                    cadena_registro = bot_replies.ALREADY_SIGNED_UP[lengua.value].format(mail_a_registrar)
                self._send_message(update, msg=cadena_registro)
            else:
                self._procesar_authorization_error(gm_api.error_code, update, lengua.value)
        else:
            sin_peticion = bot_replies.STILL_NOT_SIGNING_UP[lengua.value]. \
                format(Commands.SIGN_UP.value, CommandHelper.get(Commands.SIGN_UP).usos(lengua)[0])
            self._send_message(update, msg=sin_peticion, markdown=True)


class Delete(Command):
    def run(self, update: telegram.Update, context: telegram.ext.CallbackContext):
        usuario, chat_id, lengua = self._update_vars(update)

        if len(context.args) == 1:
            self.gestionar_arg(update, context.args[0], usuario.id, lengua.value)
        else:
            self._syntax_error(update, lengua)

    def gestionar_arg(self, update, arg, usuario, lengua: str):
        if arg == "-all":
            with Database() as db:
                if db.existen_registros(usuario):
                    db.borrar_registros(usuario)
                    self._log("El usuario {} borró todos sus registros.".format(usuario))
                    resultado = bot_replies.ALL_MAILS_DELETED[lengua]
                else:
                    resultado = bot_replies.NOT_SIGNED_UP[lengua]
        else:
            mail = MailString(arg)

            if mail.is_gmail_string():
                with Database() as db:
                    if db.existe_registro(usuario, mail):
                        db.borrar_registro(usuario, mail)
                        self._log("El usuario {} borró uno de sus registros.".format(usuario))
                        resultado = bot_replies.EMAIL_DELETED[lengua].format(mail)
                    else:
                        resultado = bot_replies.NOT_SIGNED_UP_ACCOUNT[lengua].format(mail)
            else:
                resultado = bot_replies.NOT_A_MAIL_STRING[lengua].format(mail)

        self._send_message(update, msg=resultado)


class SubscriptionChanger(Command, ABC):
    EXPECTED_ARGUMENTS = None

    def run(self, update: telegram.Update, context: telegram.ext.CallbackContext):
        usuario, _, lengua = self._update_vars(update)
        self._procesar_args(update, context, usuario.id, lengua)

    def _procesar_args(self, update, context, usuario, lengua: Languages):
        if len(context.args) == self.EXPECTED_ARGUMENTS:
            self._procesar_mail(update, context, usuario, lengua.value)
        else:
            self._syntax_error(update, lengua)

    def _procesar_mail(self, update, context, usuario, lengua: str):
        mail = MailString(context.args[0])
        if mail.is_gmail_string():
            resultado = self.__procesar_registro(update, context, usuario, mail, lengua)
        else:
            resultado = bot_replies.NOT_A_MAIL_STRING[lengua].format(mail)
        self._send_message(update, msg=resultado)

    @abstractmethod
    def _modificar_suscripcion(self, update, context, db, lengua: str, chat_id):
        pass

    def __procesar_registro(self, update, context, usuario, mail, lengua: str):
        chat_id = update.effective_chat.id
        with Database() as db:
            if db.existe_registro(usuario, mail):
                resultado = self._modificar_suscripcion(update, context, db, lengua, chat_id)
            else:
                resultado = bot_replies.UNAUTHORIZED_SUBSCRIPTION[lengua].format(mail)
        return resultado


class SubscriptionsThread(RestartableThread):
    def __init__(self, *args, **kwargs):
        self.__messenger: Messenger = args[0]
        self.periodic_check = PeriodicCheck(self.__messenger)

        super().__init__(target=self.periodic_check.execute)
        self.args = args
        self.kwargs = kwargs

    def is_active(self):
        self.periodic_check.is_active()

    def flag(self):
        self.periodic_check.comprobar_suscripciones()


class Subscribe(SubscriptionChanger):
    EXPECTED_ARGUMENTS = 3

    def __init__(self, messenger: Messenger, notif_thread: SubscriptionsThread):
        super().__init__(messenger)
        self.__notif_thread = notif_thread

        # Patterns allowed:
        #       -t <DD>
        #       -t <HH>:<MM>
        #       -h <HH>:<MM>
        hour_pattern = r"-h\s(?P<hour>[0-9]+):(?P<minute>[0-9]+)"
        time_pattern = r"-t\s(?:(?P<days>[0-9]+)|(?P<hours>[0-9]+):(?P<minutes>[0-9]+))"
        self.pattern = re.compile(r"\A(?:" + time_pattern + r")|(?:" + hour_pattern + r")\Z")

    def _modificar_suscripcion(self, update, context, db, lengua: str, chat_id):
        mail, tipo, tiempo = context.args
        mail = MailString(mail)

        frequency = self.__procesar_timestring(tipo, tiempo)

        try:
            if db.insertar_suscripcion(chat_id, mail, frequency, update, context):
                if not self.__notif_thread.is_active():
                    self.__notif_thread = self.__notif_thread.restart()
                else:
                    self.__notif_thread.flag()
                cadena_suscripcion = bot_replies.SUBSCRIPTION_COMPLETED[lengua].format(mail)
            else:
                cadena_suscripcion = bot_replies.ALREADY_SUBSCRIBED[lengua].format(mail)
        except AssertionError:
            cadena_suscripcion = bot_replies.BAD_TIME[lengua]
        return cadena_suscripcion

    def __procesar_timestring(self, tipo, tiempo):
        match = self.pattern.match(tipo + " " + tiempo)
        if match:
            frequency = {key: int(value) for key, value in match.groupdict().items() if value}

            tipo_t = {'days', 'hours', 'minutes'} & set(frequency)
            tipo_h = {'hour', 'minute'} & set(frequency)

            if tipo_t or (tipo_h and
                          0 <= frequency['hour'] <= 23 and 0 <= frequency['minute'] <= 59):
                frequency['tipo'] = 't' if tipo_t else 'h'
                return frequency
            else:
                return None
        else:
            return None


class Unsubscribe(SubscriptionChanger):
    EXPECTED_ARGUMENTS = 1

    def _modificar_suscripcion(self, _, context, db, lengua: str, chat_id):
        mail = MailString(context.args[0])

        if db.existe_suscripcion(chat_id, mail):
            db.borrar_suscripcion(chat_id, mail)
            cadena_desuscripcion = bot_replies.UNSUBSCRIPTION_COMPLETED[lengua].format(mail)
        else:
            cadena_desuscripcion = bot_replies.ALREADY_UNSUBSCRIBED[lengua].format(mail)
        return cadena_desuscripcion


class Check(Command):
    def run(self, update: telegram.Update, context: telegram.ext.CallbackContext):
        _, _, lengua = self._update_vars(update)

        if len(context.args) > 0:
            self.__procesar_mail(update, context, lengua.value)
        else:
            self._syntax_error(update, lengua)

    def _asignar_kwargs(self, chat_id, context):
        kwargs = {
            'chat_id': chat_id,
        }

        if len(context.args) == 2:
            kwargs['num_correos'] = int(context.args[1])
        return kwargs

    def __procesar_mail(self, update, context, lengua: str):
        mail = MailString(context.args[0])
        if mail.is_gmail_string():
            self.__procesar_permisos(update, context, mail)
        else:
            self._send_message(update, msg=bot_replies.NOT_A_MAIL_STRING[lengua].format(mail))

    def __procesar_permisos(self, update, context, mail):
        usuario, chat_id, lengua = self._update_vars(update)

        with Database() as db:
            registrado = db.existe_registro(usuario.id, mail)

        if registrado:
            gm_api = GmailApi(usuario.id, mail)
            if gm_api.autenticado:
                self.__procesar_correos(update, context, mail, chat_id, lengua.value, gm_api)
            else:
                self._procesar_authorization_error(gm_api.error_code, update, lengua.value, mail)
        else:
            self._send_message(update, msg=bot_replies.UNAUTHORIZED_CHECK[lengua.value].format(mail))

    def __procesar_correos(self, update, context, mail, chat_id, lengua: str, gm_api):
        kwargs = self._asignar_kwargs(chat_id, context)

        correos = list(gm_api.procesar_correos(**kwargs))
        if not correos:
            if self.__class__ is not PeriodicCheck:
                self._send_message(update, msg=bot_replies.NO_NEW_MAILS[lengua])
        elif None in correos:
            cadena_superado_limite = bot_replies.OVER_LIMIT_MESSAGES[lengua]. \
                format(MAX_CORREOS, CommandHelper.get(Commands.CHECK).usos(lengua)[0])
            self._send_message(update, msg=cadena_superado_limite, markdown=True)
        else:
            usuario = update.message.from_user.id
            self._log(
                "Se obtuvieron {} correos mediante {}.\n"
                "    chat: {}, usuario: {}.".format(
                    len(correos), self.__class__.__name__, chat_id, usuario)
            )

            if 'num_correos' not in kwargs:
                self._send_message(
                    update,
                    msg=bot_replies.NUMBER_OF_MESSAGES(len(correos), lengua)
                )
            for correo in correos:
                self._send_message(update, msg=correo.__str__(lengua), mail=mail, id=correo.id)


class PeriodicCheck(Check):
    def __init__(self, messenger: Messenger):
        super().__init__(messenger)
        self.__scheduler = BackgroundScheduler()
        self.__scheduler.start()

    def _asignar_kwargs(self, chat_id, context):
        kwargs = {
            'chat_id': chat_id,
            'disable_max': True,
        }

        return kwargs

    def _send_message(self, update, **kwargs):
        kwargs["reply"] = False
        super(Check, self)._send_message(update, **kwargs)

    def is_active(self):
        return self.__scheduler.running

    def comprobar_suscripciones(self):
        self.__scheduler.add_job(func=self.execute)

    def execute(self):
        with Database() as db:
            suscripcion = db.get_siguiente_suscripcion()

        if suscripcion and not self.__suscripcion_ya_atendida(suscripcion):
            notificacion_time = max(suscripcion["time"], datetime.now())
            self.__scheduler.add_job(func=self.__entregar_suscripcion, args=(suscripcion,), run_date=notificacion_time)

    def __suscripcion_ya_atendida(self, suscripcion):
        for job in self.__scheduler.get_jobs():
            suscripcion_atendida = job.args[0]
            if all([suscripcion[key] == suscripcion_atendida[key]
                    for key in ['chat_id', 'email', 'time']]):
                return True
        return False

    def __entregar_suscripcion(self, suscripcion):
        chat_id = suscripcion["chat_id"]
        mail = MailString(suscripcion["email"])
        update = suscripcion["update"]
        context = suscripcion["context"]

        with Database() as db:
            if db.existe_suscripcion(chat_id, mail):
                self.run(update, context)
                db.actualizar_suscripcion(chat_id, mail)

        self.__scheduler.add_job(func=self.execute)


class Echo(Command):
    def run(self, update: telegram.Update, context: telegram.ext.CallbackContext):
        usuario, chat_id, lengua = self._update_vars(update)
        self._send_message(update, msg=bot_replies.ECHO_REPLY[lengua.value])


class Unknown(Command):
    def run(self, update: telegram.Update, context: telegram.ext.CallbackContext):
        usuario, chat_id, lengua = self._update_vars(update)

        mensaje_recibido = str(update.message.text)
        self._send_message(update, msg=bot_replies.INVALID_COMMAND_REPLY[lengua.value])
        self._log(
            "El usuario {} introdujo el siguiente comando inválido: {}".format(
                self._mencion_usuario(usuario), mensaje_recibido
            )
        )


if __name__ == "__main__":
    pass
