import os
import pandas as pd
from enum import Enum
from abc import ABC, abstractmethod

from gomteg.replies.languages import Languages
from gomteg.defs import DATA_PATH

cmd_descs = pd.read_csv(os.path.join(DATA_PATH, 'commands_descriptions.csv'))
cmd_args = pd.read_csv(os.path.join(DATA_PATH, 'command_arguments.csv'))
for fila in cmd_args.iterrows():
    locals()[fila[1]['id']] = dict(fila[1][[lengua.value for lengua in list(Languages)]])


class Commands(Enum):
    START = "start"
    HELP = "help"
    SIGN_UP = "signup"
    DELETE = "delete"
    CONTACT = "contact"
    SET_LANGUAGE = "setlanguage"
    SUBSCRIBE = "subscribe"
    UNSUBSCRIBE = "unsubscribe"
    CHECK = "check"


TIME = {}
for lengua in list(Languages):
    TIME[lengua.value] = [
        HOUR_WORD[lengua.value],
        MINUTE_WORD[lengua.value]
    ]

TIMES = {}
for lengua in list(Languages):
    TIMES[lengua.value] = [
        DAYS_WORD[lengua.value],
        HOURS_WORD[lengua.value],
        MINUTES_WORD[lengua.value]
    ]


class CommandHelp(ABC):
    @abstractmethod
    def usos(self, lengua: str):
        pass

    def _get_description(self):
        command_row = cmd_descs.loc[lambda commands_desc: commands_desc['id'] == self.command.value, :]
        return command_row[[lengua.value for lengua in list(Languages)]].to_dict('records')[0]

    def help(self, lengua: Languages):
        string = USAGES_TITLE[lengua.value]
        for uso in self.usos(lengua.value):
            string += "\n\t*{}*".format(uso)
        return string


class HelpCH(CommandHelp):
    def __init__(self):
        self.command = Commands.HELP
        self.descripcion = self._get_description()
        self.args = [COMMAND]

    def usos(self, lengua: str):
        return [
            "/{} [{}]".format(self.command.value, self.args[0][lengua])
        ]


class SignUpCH(CommandHelp):
    def __init__(self):
        self.command = Commands.SIGN_UP
        self.descripcion = self._get_description()
        self.args = [CODE]

    def usos(self, lengua: str):
        return [
            "/{}".format(self.command.value),
            "/{} <{}>".format(self.command.value, self.args[0][lengua])
        ]


class DeleteCH(CommandHelp):
    def __init__(self):
        self.command = Commands.DELETE
        self.descripcion = self._get_description()
        self.args = [MAIL]

    def usos(self, lengua: str):
        return [
            "/{} <{}>".format(self.command.value, self.args[0][lengua]),
            "/{} -all".format(self.command.value)
        ]


class ContactCH(CommandHelp):
    def __init__(self):
        self.command = Commands.CONTACT
        self.descripcion = self._get_description()
        self.args = [MESSAGE]

    def usos(self, lengua: str):
        return ["/{} <{}>".format(self.command.value, self.args[0][lengua])]


class SetLanguageCH(CommandHelp):
    def __init__(self):
        self.command = Commands.SET_LANGUAGE
        self.descripcion = self._get_description()

    def usos(self, lengua: str):
        return ["/{} <es|en|mwl>".format(self.command.value)]


class SubscribeCH(CommandHelp):
    def __init__(self):
        self.command = Commands.SUBSCRIBE
        self.descripcion = self._get_description()
        self.args = [MAIL, TIMES, TIME]

    def usos(self, lengua: str):
        return [
            "/{} <{}> -t <{}>".format(self.command.value, self.args[0][lengua], self.args[1][lengua][0]),
            "/{} <{}> -t <{}:{}>".format(self.command.value, self.args[0][lengua], *self.args[1][lengua][1:]),
            "/{} <{}> -h <{}:{}>".format(self.command.value, self.args[0][lengua], *self.args[2][lengua])
        ]


class UnsubscribeCH(CommandHelp):
    def __init__(self):
        self.command = Commands.UNSUBSCRIBE
        self.descripcion = self._get_description()
        self.args = [MAIL]

    def usos(self, lengua: str):
        return [
            "/{} <{}>".format(self.command.value, self.args[0][lengua])
        ]


class CheckCH(CommandHelp):
    def __init__(self):
        self.command = Commands.CHECK
        self.descripcion = self._get_description()
        self.args = [MAIL, NUMBER]

    def usos(self, lengua: str):
        return [
            "/{} <{}> [{}]".format(self.command.value, self.args[0][lengua], self.args[1][lengua])
        ]


class CommandHelper(Enum):
    HELP = HelpCH()
    SIGN_UP = SignUpCH()
    DELETE = DeleteCH()
    CONTACT = ContactCH()
    SET_LANGUAGE = SetLanguageCH()
    SUBSCRIBE = SubscribeCH()
    UNSUBSCRIBE = UnsubscribeCH()
    CHECK = CheckCH()

    @staticmethod
    def get(comm: Commands):
        helpers = {
            Commands.HELP: CommandHelper.HELP.value,
            Commands.SIGN_UP: CommandHelper.SIGN_UP.value,
            Commands.DELETE: CommandHelper.DELETE.value,
            Commands.CONTACT: CommandHelper.CONTACT.value,
            Commands.SET_LANGUAGE: CommandHelper.SET_LANGUAGE.value,
            Commands.SUBSCRIBE: CommandHelper.SUBSCRIBE.value,
            Commands.UNSUBSCRIBE: CommandHelper.UNSUBSCRIBE.value,
            Commands.CHECK: CommandHelper.CHECK.value
        }

        return helpers.get(comm)
