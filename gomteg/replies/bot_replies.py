import os
import pandas as pd

from gomteg.replies.languages import Languages
from gomteg.defs import DATA_PATH

csv = pd.read_csv(os.path.join(DATA_PATH, 'bot_replies.csv'))
for fila in csv.iterrows():
    locals()[fila[1]['id']] = dict(fila[1][[lengua.value for lengua in list(Languages)]])


ARGUMENT_TYPES = {}
for lengua in list(Languages):
    ARGUMENT_TYPES[lengua.value] = [ARGUMENT_TYPE_1[lengua.value], ARGUMENT_TYPE_2[lengua.value]]


def NUMBER_OF_MESSAGES(num_correos: int, lengua: str):
    if num_correos == 1:
        return ONE_UNREAD_MESSAGE[lengua]
    else:
        return PLURAL_NUMBER_OF_MESSAGES[lengua].format(num_correos)
