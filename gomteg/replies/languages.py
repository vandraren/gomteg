from enum import Enum


class Languages(Enum):
    ENGLISH = "en"
    CASTELLANO = "es"
    LLIONES = "mwl"
